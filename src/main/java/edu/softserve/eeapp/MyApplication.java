package edu.softserve.eeapp;

import edu.softserve.eeapp.services.*;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.glassfish.jersey.server.ResourceConfig;

import javax.inject.Singleton;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("v1")
public class MyApplication extends ResourceConfig {

    public MyApplication() {

        packages("edu.softserve.eeapp.api");
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bindAsContract(BigService.class);
                bindAsContract(CounterService.class).in(RequestScoped.class);
                bind(SingletonService.class).named("Singleton").to(IService.class).in(Singleton.class);
                bind(PerLookUpService.class).named("perlookup").to(IService.class);
            }
        });

    }
}
