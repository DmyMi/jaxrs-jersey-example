package edu.softserve.eeapp.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("ping")
@Produces({"text/plain"})
public class Ping {

    @GET
    public String get() {
        return "pong";
    }
}