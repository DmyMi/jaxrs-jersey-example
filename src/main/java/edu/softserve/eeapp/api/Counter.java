package edu.softserve.eeapp.api;

import edu.softserve.eeapp.dto.RequestDTO;
import edu.softserve.eeapp.dto.ResponseDTO;
import edu.softserve.eeapp.services.BigService;
import edu.softserve.eeapp.services.CounterService;
import edu.softserve.eeapp.services.IService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("counter")
@Produces(MediaType.APPLICATION_JSON)
public class Counter {

    private CounterService perRequest;
    private BigService bigsvc;
    private IService singleton;
    private IService perLookup;

    @Inject
    public Counter(CounterService perRequest, @Named("Singleton") IService singleton, @Named("perlookup") IService perLookup, BigService bigsvc) {
        this.perRequest = perRequest;
        this.singleton = singleton;
        this.perLookup = perLookup;
        this.bigsvc = bigsvc;
    }

    @GET
    @Path("request")
    public Response counter() {
        ResponseDTO res = new ResponseDTO(perRequest.getCounter() + bigsvc.getRequestScoped());
        GenericEntity<ResponseDTO> entity = new GenericEntity<>(res, ResponseDTO.class);
        return Response.status(200).entity(entity).build();
    }

    @GET
    @Path("lookup")
    public Response counterv1() {
        ResponseDTO res = new ResponseDTO(perLookup.getCounter() + bigsvc.getPerLookUp());
        GenericEntity<ResponseDTO> entity = new GenericEntity<>(res, ResponseDTO.class);
        return Response.status(200).entity(entity).build();
    }

    @GET
    @Path("singleton")
    public Response counterv2() {
        ResponseDTO res = new ResponseDTO(singleton.getCounter() + bigsvc.getSingleton());
        GenericEntity<ResponseDTO> entity = new GenericEntity<>(res, ResponseDTO.class);
        return Response.status(200).entity(entity).build();
    }

    @POST
    @Path("minus")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response minus(RequestDTO req) {
        singleton.minus(req.getMinus());
        return Response.status(200).build();
    }
}
