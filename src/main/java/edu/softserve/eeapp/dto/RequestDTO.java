package edu.softserve.eeapp.dto;

public class RequestDTO {
        private int minus;

        public RequestDTO(int minus) {
                this.minus = minus;
        }

        public int getMinus() {
                return minus;
        }

        public void setMinus(int minus) {
                this.minus = minus;
        }
}