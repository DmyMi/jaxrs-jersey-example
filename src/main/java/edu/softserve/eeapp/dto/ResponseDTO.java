package edu.softserve.eeapp.dto;

public class ResponseDTO {
    private int data;

    public ResponseDTO(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}