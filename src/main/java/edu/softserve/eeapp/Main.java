package edu.softserve.eeapp;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.eclipse.jetty.servlet.ServletContextHandler.NO_SESSIONS;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        MyApplication application = new MyApplication();

        ServletHolder jerseyServlet
                = new ServletHolder(new ServletContainer(application));


        Server server = new Server(8080);

        ServletContextHandler servletContextHandler = new ServletContextHandler(NO_SESSIONS);


        servletContextHandler.setContextPath("/");
        server.setHandler(servletContextHandler);

        servletContextHandler.addServlet(jerseyServlet, "/v1/*");
//        jerseyServlet.setInitOrder(0);
//        jerseyServlet.setInitParameter(
//                "jersey.config.server.provider.packages",
//                "edu.softserve.eeapp.api"
//        );
        servletContextHandler.addServlet(DefaultServlet.class, "/");

        try {
            server.start();
            server.join();
        } catch (Exception ex) {
            logger.error("Error occurred while starting Jetty", ex);
            System.exit(1);
        } finally {
            server.destroy();
        }
    }
}
