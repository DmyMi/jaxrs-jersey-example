package edu.softserve.eeapp.services;

import javax.inject.Inject;
import javax.inject.Named;

public class BigService {

    private CounterService perRequest;
    private IService singleton;
    private IService perLookup;

    @Inject
    public BigService(CounterService perRequest, @Named("Singleton") IService singleton, @Named("perlookup") IService perLookup) {
        this.perRequest = perRequest;
        this.singleton = singleton;
        this.perLookup = perLookup;
    }

    public int getRequestScoped() {
        return perRequest.getCounter();
    }

    public int getPerLookUp() {
        return perLookup.getCounter();
    }

    public int getSingleton() {
        return singleton.getCounter();
    }
}
