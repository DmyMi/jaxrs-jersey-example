package edu.softserve.eeapp.services;

import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;

@Service
@Named
public class CounterService implements IService {

    private static final Logger logger = LoggerFactory.getLogger(CounterService.class);

    private int counter = 0;

    public int getCounter() {
        this.counter +=1;
        logger.info(String.format("invoking a counter method in singleton class with incremented counter: %d", counter));
        return this.counter;
    }
}
