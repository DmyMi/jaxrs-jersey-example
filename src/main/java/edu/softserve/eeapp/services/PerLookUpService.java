package edu.softserve.eeapp.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PerLookUpService implements IService {

    private static final Logger logger = LoggerFactory.getLogger(PerLookUpService.class);

    private int counter = 0;

    public int getCounter() {
        this.counter +=1;
        logger.info(String.format("invoking a counter method in singleton class with incremented counter: %d", counter));
        return this.counter;
    }
}
