package edu.softserve.eeapp.services;

import org.jvnet.hk2.annotations.Contract;

@Contract
public interface IService {
    int getCounter();

    default void minus(int value) {

    }
}
