package edu.softserve.eeapp.services;

import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;

@Service @Named
public class SingletonService implements IService {

    private static final Logger logger = LoggerFactory.getLogger(SingletonService.class);

    private int counter = 0;

    public int getCounter() {
        counter +=1;
        logger.info(String.format("invoking a counter method in singleton class with incremented counter: %d", counter));
        return counter;
    }

    @Override
    public void minus(int value) {
        counter -= value;
        logger.info(String.format("invoking a minus method in singleton class with decremented counter: %d", counter));
    }
}
